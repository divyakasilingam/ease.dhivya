import React from 'react'
import { Switch, Route } from 'react-router-dom';
import Header from '../Layout/Header'
import Menu from '../Home/Menu/Menu'
import BILL from '../Home/Pages/Bill/Bill'
import PROFILES from '../Home/Pages/Profiles/Profiles';
import PROJECTS from '../Home/Pages/Projects/Projects';
import ADMIN from '../Home/Pages/Admin/Admin'
import './Home.css'

const Home = (props) => {

  return (
    <div>
      <Header route={props} />
      <div className="pages">
       <Switch>
        <Route path={props.match.url} exact component={Menu} />
        <Route path={props.match.url+'/BIll'}  component={BILL} />
        <Route path={props.match.url+'/PROFILES'}  component={PROFILES} />
        <Route path={props.match.url+'/PROJECTS'}  component={PROJECTS} />
        <Route path={props.match.url+'/ADMIN'}  component={ADMIN} />
      </Switch>
      </div>
    </div>
  )


}

export default Home
