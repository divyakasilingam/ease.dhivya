import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Grow from '@material-ui/core/Grow';
import axios from 'axios';

const useStyles = makeStyles({
  card: {
    maxWidth: 345,
    maxHeight:600
  },
});

const Profiles = () => {
  const [state, setstate] = useState([]);
  const classes = useStyles();
  let checked = true
  
  useEffect(() => {
    axios.get('https://jsonplaceholder.typicode.com/posts')
      .then(function (response) {
        console.log(response);
        setstate(response.data)
      }).catch(err => {
        console.log(err)
      })

  }, []);


  const profile = state.map((x) => {
    return (
      <Grid item xs={3} key={x.id}>
        <Grow in={checked} style={{ transformOrigin: '0 0 0',transitionTimingTunction:'ease-in' }}
          {...(checked ? { timeout: 900 } : {})}>
          <Card className={classes.card}>
            <CardActionArea>
              <CardMedia
                component="img"
                alt={x.id}
                height="250"
                image="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRztGdBeduqRviewEXPNsUhnDmRpW1Y4V7FgS_Xqh6GD0AYSRLW"
                title={x.id}
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  {x.title}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  {x.body}
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Button size="small" color="primary">
                follow
        </Button>
              <Button size="small" color="primary">
                More Details
        </Button>
            </CardActions>
          </Card>
        </Grow>
      </Grid>
    )
  })

    return (
      <div className="detailscontainer">
        <Grid container spacing={3}>
          {profile}
        </Grid>
      </div>
    );
  

}

export default Profiles
