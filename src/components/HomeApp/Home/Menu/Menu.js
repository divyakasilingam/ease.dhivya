import React from 'react'
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { Link } from 'react-router-dom';
import Fade from '@material-ui/core/Fade';
import './Menu.css'

const Menu = (props) => {
  let route = props.match.url;
  let checked=true;
  let menuDetails = [
    { id: 1, name: 'ADMIN' }, { id: 2, name: 'PROFILES' }, { id: 3, name: 'PROJECTS' },
    { id: 4, name: 'HOLIDAYS' }, { id: 5, name: 'WORKSHOPS' },
    { id: 6, name: 'INTERSHIPS' }, { id: 7, name: 'BLOG' },
    { id: 8, name: 'NEWS' }
  ]

  let Details = menuDetails.map((x) => {
    return (
      <Grid item xs={3} key={x.id}>
        <Link to={route + `/` + x.name} className="links">
        <Fade in={checked}  style={{ transformOrigin: '0 0 0' }}
          {...(checked ? { timeout: 1000 } : {})}>
          <Paper className="boxDetails">
            <div className="overlay">
              <div className="text">{x.name}</div>
            </div>
          </Paper>
         </Fade>
        </Link>
      </Grid>
    )
  })

  return (
    <div>
      <Grid className="detailsContainer" container spacing={3}>
        {Details}
      </Grid>
    </div>
  )

}
export default Menu